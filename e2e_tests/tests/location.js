describe("Test to verify Chicago location details in footer", function(){

    before(function(browser){
        browser.url("https://www.designory.com/");
        browser.click('.button');
    });

    test("To Verify that the Chicago location is present in the footer",(browser) =>{
        browser.moveToElement('.location',100,100);
        browser.expect.element('.location:nth-of-type(2)').text.to.contains('CHICAGO');
        browser.expect.element('.location:nth-of-type(2)').text.to.contains('+1 312 729 4500');
        browser.assert.cssProperty(".location:nth-of-type(2)> .location-content > .location-title > a", "color", 'rgba(128, 128, 132, 1)');
        browser.moveToElement(".location:nth-of-type(2)> .location-content > .location-title > a",10,10);
        browser.assert.cssProperty(".location:nth-of-type(2)> .location-content > .location-title > a", "color", 'rgba(255, 255, 255, 1)');
    });

    test("To Verify that when a location is clicked, the user is taken to the relevant page",(browser) =>{
        browser.moveToElement('.location',100,100);
        browser.click('.location:nth-of-type(2)');
        browser.assert.urlContains('/locations/chicago');
    });

    test("To Verify that the same location in the footer now has an active state",(browser) =>{
        browser.moveToElement('.location',100,100);
        browser.click('.location:nth-of-type(2)');
        browser.moveToElement('.location',100,100);
        browser.assert.cssClassPresent(".location:nth-of-type(2)", "current");
    });
});