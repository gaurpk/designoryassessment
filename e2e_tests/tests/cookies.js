describe("Test to verify cookie notice functionality", function() {

    beforeEach(function(browser){
        browser.url("https://www.designory.com/");
    });

    afterEach(function(browser){
        browser.end();
    });

    test("To verify that user doesn't see the notice after accepting the cookie notice",(browser)=>{
        browser.assert.visible(".cookie-notice");
        browser.click('.button');
        browser.assert.not.visible(".cookie-notice");
    });

    test("To Verify that user doesn't see the notice after closing the cookie notice",(browser)=>{
        browser.assert.visible(".cookie-notice");
        browser.click('.cookie-notice__close');
        browser.assert.not.visible(".cookie-notice");
    });

    test("To Verify that after clearing cookies the “cookie notice” shows up again",(browser)=>{
        browser.assert.visible(".cookie-notice");
        browser.click('.cookie-notice__close');
        browser.assert.not.visible(".cookie-notice");
        browser.deleteCookies();
        browser.refresh();
        browser.assert.visible(".cookie-notice");
    });
});