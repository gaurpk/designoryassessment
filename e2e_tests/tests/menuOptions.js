describe("Test to verify menu options", function() {

    var assertMenuOptions = (browser) => {
        browser.click(".header-toggle");
        browser.expect.elements('nav > ul > li').count.to.equal(6);
        browser.assert.containsText("nav > ul > li:nth-of-type(1)", "WORK");
        browser.assert.containsText("nav > ul > li:nth-of-type(2)", "ABOUT");
        browser.assert.containsText("nav > ul > li:nth-of-type(3)", "CAREERS");
        browser.assert.containsText("nav > ul > li:nth-of-type(4)", "LOCATIONS");
        browser.assert.containsText("nav > ul > li:nth-of-type(5)", "CONTACT");
        browser.assert.containsText("nav > ul > li:nth-of-type(6)", "NEWS");
    };

    before(function(browser){
        browser.url("https://www.designory.com/");
    });

    test("To verify that the menu contains six options",(browser)=>{
        assertMenuOptions(browser);
    });

    test("To verify that relevant page opens on clicking menu options and check menu options",(browser)=>{
        browser.click(".header-toggle");
        //navigate to work page
        browser.click("nav > ul > li:nth-of-type(1)");
        browser.assert.urlContains('/work');
        assertMenuOptions(browser);
        //navigate to about page
        browser.click("nav > ul > li:nth-of-type(2)");
        browser.assert.urlContains('/about');
        assertMenuOptions(browser);
        //navigate to careers page
        browser.click("nav > ul > li:nth-of-type(3)");
        browser.assert.urlContains('/careers');
        assertMenuOptions(browser);
        //navigate to long-beach location page
        browser.click("nav > ul > li:nth-of-type(4)");
        browser.click(".subnav > li:nth-of-type(1)");
        browser.assert.urlContains('/locations/long-beach');
        assertMenuOptions(browser);
        //navigate to contact page
        browser.click("nav > ul > li:nth-of-type(5)");
        browser.assert.urlContains('/contact');
        assertMenuOptions(browser);
        //navigate to news page
        browser.click("nav > ul > li:nth-of-type(6)");
        browser.assert.urlContains('/news');
        assertMenuOptions(browser);
    });
});