# DesignoryAssessment

AE Technical Evaluation

Steps to run the test suite:
1. Clone the repository to your local machine
2. In the command line, change the directory to the cloned project location (Name of the project is designoryassessment)
3. Move to e2e_tests folder (cd e2e_tests)
4. Run the following command (npx nightwatch -e chrome)